import { BullsPage } from './app.po';

describe('bulls App', () => {
  let page: BullsPage;

  beforeEach(() => {
    page = new BullsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
