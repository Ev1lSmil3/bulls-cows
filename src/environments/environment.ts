// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  SERVER_HOST : '',
  SERVER_PORT : 8888,
  firebase: {
    apiKey: "AIzaSyDTS7CmP9PphCOM_PTH9jnUEG60lr6yGKI",
    authDomain: "bull-cows.firebaseapp.com",
    databaseURL: "https://bull-cows.firebaseio.com",
    projectId: "bull-cows",
    storageBucket: "bull-cows.appspot.com",
    messagingSenderId: "59650055713"
  } 
};
