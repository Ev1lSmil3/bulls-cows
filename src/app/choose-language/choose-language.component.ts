import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LanguageService } from '../lang/language.service'

@Component({
  selector: 'app-choose-language',
  templateUrl: './choose-language.component.html',
  styleUrls: ['./choose-language.component.css']
})

export class ChooseLanguageComponent implements OnInit {

  constructor(private modalService: NgbModal, public activeModal: NgbActiveModal, private languageService: LanguageService) { }

  ngOnInit() {
  }

  langEng() {
    localStorage.setItem('lang', 'en');
    this.languageService.lang = 'en';
    this.activeModal.close('Close click')
  }

  langRus() {
    localStorage.setItem('lang', 'ru');
    this.languageService.lang = 'ru';
    this.activeModal.close('Close click')
  }

}
