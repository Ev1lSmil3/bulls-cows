import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../environments/environment';
import { ValidationService } from '../utilities/validation/validation.service';
import { UserSignUpData } from './usersignupdata';
import { FormsModule } from '@angular/forms';
import { Validity } from '../utilities/validation/validity';
import { ChooseLanguageComponent } from '../choose-language/choose-language.component';
import { LanguageService } from '../lang/language.service'
import { AuthService } from '../user/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {

  userData: UserSignUpData = {
    nickName: '',
    email: '',
    password: ''
  };

  focusOutNick: boolean = false;
  focusOutEmail: boolean = false;
  focusOutPass: boolean = false;
  focusOutRetryPass: boolean = false;
  tmpStr: string = this.tmpStr;
  resultSignUp = '';

  constructor(private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private validationService: ValidationService,
    private languageService: LanguageService,
    private authService: AuthService,
    private afa: AngularFireAuth) { }

  ngOnInit() {
  }
  signUp() {
    if (this.validationService.validateString(this.userData.nickName, { minLength: 3, maxLength: 12, clearSpaces: true }).validity &&
      this.validationService.validateString(this.userData.email, { isMail: true }).validity &&
      this.validationService.validateString(this.userData.password, { minLength: 8, maxLength: 12, clearSpaces: true }).validity &&
      this.tmpStr == this.userData.password)
      this.authService.signUp(this.userData)
                      .then(data => { this.resultSignUp = this.languageService.get('resultSignUp'); setTimeout(this.activeModal.close('Close click'), 3000) }, err => {this.resultSignUp = err});
  }


}
