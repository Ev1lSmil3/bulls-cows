import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService} from '../utilities/validation/validation.service';
import { LanguageService } from '../lang/language.service';
import { AuthService } from '../user/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email;
  pass;
  resultSignIn = '';
  constructor(private modalService: NgbModal, 
              public activeModal: NgbActiveModal,
              private validationService : ValidationService,
              private languageService: LanguageService,
              private authService : AuthService,
              private afa : AngularFireAuth) { 
    
  }
  @Input('test') test;
   signIn() {
      this.authService.singIn(this.email, this.pass).then(data => {this.activeModal.close('Close click')}, err => {this.resultSignIn = err;})
  }
  ngOnInit() {
  }
  

}
