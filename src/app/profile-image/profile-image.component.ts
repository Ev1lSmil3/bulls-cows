import { Component, OnInit, ViewChild, Input} from '@angular/core';
import { CropperSettings } from 'ng2-img-cropper';
import { FirebaseApp } from 'angularfire2';

@Component({
  selector: 'app-profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.css']
})
export class ProfileImageComponent implements OnInit {
  cropperSettings: CropperSettings;
  imgError;
  originalImageFile;
  data: any= {}; 
  result;
  @Input('ref') ref;
  @ViewChild('cropper') cropper: any;
  constructor(private firebaseApp: FirebaseApp) { }

  ngOnInit() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 200;
    this.cropperSettings.height = 200;
    this.cropperSettings.keepAspect = false;
    this.cropperSettings.croppedWidth = 300;
    this.cropperSettings.croppedHeight = 300;

    this.cropperSettings.canvasWidth = 460;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 200;
    this.cropperSettings.minHeight = 200;

    this.cropperSettings.rounded = true;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,125,125,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
  }
  onImageSelected = (event) => {
    this.imgError = '';
    
    this.originalImageFile = event.srcElement.files[0];
    if (!this.originalImageFile) {
      this.imgError = 'Error loading image';
      return;
    }

    if (this.originalImageFile.size > 2048000 ) {
       this.imgError = 'Image should be less than 2 mb';
       return;
    }

    const reader = new FileReader();

    reader.onload = e => {
      const cropperImage = new Image();
      cropperImage.name = this.originalImageFile.name;

      cropperImage.onload = () => {
        // TODO We need to discuss these requirements
        if (cropperImage.width > 2000 || cropperImage.height > 2000) {
          this.imgError = 'width and heigth must be less than 1000x1000';
          return;
        }

        if (cropperImage.width < 180 || cropperImage.height < 180) {
          this.imgError = 'Width and height must be more than 180x180';
          return;
        }

        this.cropper.setImage(cropperImage);
        
      };

      cropperImage.src = e.target['result'];
    };

    reader.readAsDataURL(this.originalImageFile);
  }

   confirm() {
    const split = this.data.image.split(',');
    const byteString = atob(split[1]);
    let n = byteString.length;
    const u8Array = new Uint8Array(n);
    while (n--) {
      u8Array[n] = byteString.charCodeAt(n);
    }
    const croppedImageFile = new File([u8Array], this.originalImageFile.name, {type: this.originalImageFile.type});
    const croppedImageSource = this.data.image;
    this.result = {croppedImageFile: croppedImageFile, croppedImageSource: croppedImageSource};
    this.ref.close(this.result);
  }

}
