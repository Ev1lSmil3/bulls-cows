import { Injectable } from '@angular/core';
import { StringValidation } from './string-validation';
import { Validity } from './validity';
import { LanguageService } from '../../lang/language.service';
@Injectable()
export class ValidationService {

  constructor(private languageService: LanguageService) { }

  private isMail(email: string): boolean {
    let re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  clearString(str: string): string {
    return str.replace(/\s\s+/g, ' ').trim();
  }

  random(number: number): string {
    var array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    var i = array.length,
      j = 0,
      temp;

    while (i--) {

      j = Math.floor(Math.random() * (i + 1));

      temp = array[i];
      array[i] = array[j];
      array[j] = temp;

    }

    return array.slice(0, number).join('');

  }

  checkNumber(number) {
    let validity = false;
    if (this.isNumeric(number) && number.length == 4) {
      validity = true;
      for (let j = 0; j < number.length; j++) {
        for (let i = j; i < number.length - 1; i++) {
          if (number[j] == number[i + 1]) {
            validity = false;
            break;
          }
        }
      }
    }
    return validity;
  }

  isNumeric(value) {
    return /^\d+$/.test(value);
  }
  
  checkTryNumber(number) {
    let validity = false;
    if (this.isNumeric(number) && number.length == 4) {
      validity = true;
    }
    return validity;
  }

  //to do 
  // make vadidation depending on params
  validateString(str: string, params: StringValidation): Validity {

    if (params.clearSpaces)
      if (/\s/g.test(str))
        return { validity: false, message: this.languageService.get('spaceAlert') };

    if (params.isMail)
      if (!this.isMail(str))
        return { validity: false, message: this.languageService.get('emailAlert') };

    if (params.minLength)
      if (str.length < params.minLength)
        return { validity: false, message: this.languageService.get('minLengthAlert') + params.minLength };

    if (params.maxLength)
      if (str.length > params.maxLength)
        return { validity: false, message: this.languageService.get('maxLengthAlert') + params.maxLength };

    return { validity: true, message: '' };

  }
}
