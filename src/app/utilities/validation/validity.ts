export interface Validity {
    validity : boolean,
    message?: string,
}
