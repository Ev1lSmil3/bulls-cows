export interface StringValidation {
    minLength? : number,
    maxLength? : number,
    clearSpaces?: boolean,
    onlyLetters? : boolean,
    isMail? : boolean,
    

}
