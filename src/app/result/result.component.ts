import { Component, OnInit } from '@angular/core';
import { GameService } from '../game/game.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from "@angular/router";
import { LanguageService } from '../lang/language.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  constructor(private gameService: GameService,
    private afdb: AngularFireDatabase,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private router: Router,
    private languageService: LanguageService) { }
  result;
  image;
  ngOnInit() {
    this.afdb.object('game/' + this.gameService.game.key + '/status').subscribe(status => {
      if (this.gameService.player == 1 && this.gameService.game.status == 'player1Won' || this.gameService.player == 2 && this.gameService.game.status == 'player2Won') {
        this.result = this.languageService.get('victory');
        this.image = 'assets/images/victory.gif';
        clearInterval(this.gameService.inter);
        delete this.gameService.game;
        delete this.gameService.player2Data;
      } else if (this.gameService.player == 1 && this.gameService.game.status == 'player2Won' || this.gameService.player == 2 && this.gameService.game.status == 'player1Won') {
        this.result = this.languageService.get('defeat');
        this.image = 'assets/images/lose.jpg';
        clearInterval(this.gameService.inter);
        delete this.gameService.game;
        delete this.gameService.player2Data;
      }
    });
  }

}
