import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserSignUpData } from '../signup/usersignupdata';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {

  constructor(private afa: AngularFireAuth,
    private afdb: AngularFireDatabase,
    private userService: UserService) { }

  signUp(userData: UserSignUpData) {
    return new Promise((resolve, reject) => {
       this.afa.auth.createUserWithEmailAndPassword(userData.email, userData.password).then(data => {
         let tmpUser = { email: userData.email, nickname: userData.nickName, id: data.uid, rank: 1500, key: '' };   
         let key = this.afdb.list('users').push(tmpUser).key;
         tmpUser.key = key;
          this.singIn(userData.email, userData.password).then(done1 => {
            this.userService.update(tmpUser).then(data => {
              resolve();
            }, err => {
              reject();
            })
            resolve('done');
          }, err => {
            reject(err);
          })

      }, err => {
        reject(err);
      })
    });
  }

  singIn(email, password) {
    return new Promise((resolve, reject) => {
      this.afa.auth.signInWithEmailAndPassword(email, password).then(data => {
        resolve('done');
      }, err => {
        reject(err);
      })
    })
  }

  logOut() {
    this.afa.auth.signOut();
  }


}
