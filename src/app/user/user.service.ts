import { Injectable, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';

@Injectable()
export class UserService implements OnInit {
  currentUser;
  currentUserData;
  userProfileImage;
  constructor(private afa: AngularFireAuth,
    private afdb: AngularFireDatabase) {
    this.afa.authState.subscribe(user => {
      this.currentUser = user;
      if (user) {
        let subscription = this.afdb.list('users', {
          query: {
            orderByChild: 'id',
            equalTo: this.currentUser.uid
          }
        }).subscribe(users => {
          users.forEach(user => {
            this.currentUserData = user;
            if (this.currentUserData.image) {
            let storageRef = firebase.storage().ref();
            storageRef.child(this.currentUserData.image).getDownloadURL().then(url => {
              this.userProfileImage = url;
            });
            }
            subscription.unsubscribe();
          });
        })
      } else {
        this.currentUserData = null;
      }
    });
  }
  update(user) {
    return new Promise((resolve, reject) => {
      this.afdb.object('/users/' + user.key).set(user).then(done => { resolve(); }, err => { reject(err); })
    });
  }
  ngOnInit() {
  }

}
