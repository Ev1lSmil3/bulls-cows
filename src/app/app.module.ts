import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
/**
 * components
 */
import { HomeComponent } from './home/home.component';
import { TestComponent } from './test/test.component';


/*  end components */
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';

//services 
import { ValidationService } from './utilities/validation/validation.service';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { AuthService } from './user/auth.service';
import { UserService } from './user/user.service';
import { SignupComponent } from './signup/signup.component';
import { FooterComponent } from './footer/footer.component';
import { ChooseLanguageComponent } from './choose-language/choose-language.component';
import { LanguageService } from './lang/language.service';
import { ProfileComponent } from './profile/profile.component';
import { ProfileImageComponent } from './profile-image/profile-image.component';
import { GameService } from './game/game.service'


import {ImageCropperModule} from 'ng2-img-cropper';
import { GameComponent } from './game/game.component';
import { TransferPipe } from './game/transfer.pipe';
import { ResultComponent } from './result/result.component';

const appRoutes: Routes = [
  { path: 'game', component: GameComponent},
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'test', component: TestComponent },
  { path: 'profile', component: ProfileComponent},
  { path: '**', component: HomeComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TestComponent,
    LoginComponent,
    HeaderComponent,
    SignupComponent,
    FooterComponent,
    ChooseLanguageComponent,
    ProfileComponent,
    ProfileImageComponent,
    GameComponent,
    TransferPipe,
    ResultComponent,

  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ImageCropperModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, 
    AngularFireAuthModule,
  ],
  providers: 
  [DialogService,
    NgbActiveModal,
    ValidationService,
    LanguageService,
    AuthService,
    UserService,
    GameService],
  bootstrap: [AppComponent],
  entryComponents: [
    HomeComponent,
    LoginComponent,
    SignupComponent,
    ChooseLanguageComponent,
    ProfileImageComponent,
    GameComponent,
    ResultComponent
  ]
})
export class AppModule { }
