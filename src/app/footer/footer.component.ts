import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../lang/language.service';
import { GameService } from '../game/game.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private languageService: LanguageService,  private gameService: GameService) { }

  ngOnInit() {
  }

}
