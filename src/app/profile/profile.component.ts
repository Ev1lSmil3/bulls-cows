import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ProfileImageComponent } from '../profile-image/profile-image.component';
import { LanguageService } from '../lang/language.service'
import { FirebaseApp } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private languageService: LanguageService,
    private firebaseApp: FirebaseApp,
    private afdb: AngularFireDatabase,
    private userService: UserService) { }
  newImage;
  isLoading = false;
  isDeleting = false;

  ngOnInit() {
  }

  changeImage() {
    const modalRef: NgbModalRef = this.modalService.open(ProfileImageComponent);
    modalRef.componentInstance.ref = modalRef;
    modalRef.result.then((newImage) => {
      this.newImage = newImage;
    }, () => 42);

  }


  upload() {
    if (this.newImage && !this.isLoading && !this.isDeleting ) {
      let storageRef = firebase.storage().ref();
      if (this.userService.currentUserData.image) {
        this.isLoading = true;
        storageRef.child(this.userService.currentUserData.image).delete().then(() => {
          storageRef.child(this.userService.currentUserData.image).put(this.newImage.croppedImageFile).then((snapshot) => {
            this.isLoading = false;
          });
        });
      }
      else {
        this.isLoading = true;
        let imageRef = 'images/' + this.userService.currentUserData.key + '/' + this.userService.currentUserData.key;
        storageRef.child(imageRef).put(this.newImage.croppedImageFile).then((snapshot) => {
          this.userService.currentUserData.image = imageRef;
          this.userService.update(this.userService.currentUserData);
          this.isLoading = false;
        });
      }
    }
  }

  delete() {
    if (this.userService.currentUserData.image && !this.isDeleting && !this.isLoading) {
      this.isDeleting = true;
      let storageRef = firebase.storage().ref();
      storageRef.child(this.userService.currentUserData.image).delete().then(() => {
        delete this.userService.currentUserData.image;
        this.userService.update(this.userService.currentUserData);
        delete this.userService.userProfileImage;
        delete this.newImage;
        this.isDeleting = false;
      });
    }
    else if (this.newImage && !this.isDeleting) {
      delete this.newImage
    }
  }

}