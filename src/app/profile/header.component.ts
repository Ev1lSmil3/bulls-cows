import { Component, OnInit } from '@angular/core';
import { DialogService } from 'ng2-bootstrap-modal';
import { ChooseLanguageComponent } from '../choose-language/choose-language.component';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private modalService: NgbModal, private activeModal: NgbActiveModal) { }

  ngOnInit() {
    if (!localStorage.getItem('lang')) {
      const modalRef = this.modalService.open(ChooseLanguageComponent);
    }
  }

  showLogin() {
    const modalRef = this.modalService.open(LoginComponent);
    modalRef.componentInstance.test = "test";
  }

  showSignUp() {
    const modalRef = this.modalService.open(SignupComponent);
  }

}
