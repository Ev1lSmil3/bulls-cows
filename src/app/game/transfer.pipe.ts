import { Pipe, PipeTransform } from '@angular/core';
import { LanguageService } from '../lang/language.service';

@Pipe({
  name: 'transfer'
})
export class TransferPipe implements PipeTransform {
  constructor (private languageService: LanguageService) {}

  transform(number1: string, number2: string): string {
    let bulls = 0;
    let cows = 0;
    for (let i = 0; i < number1.length; i++) {
      if (number1[i] == number2[i]) {
        bulls++;
      } else if (number1.indexOf(number2[i]) >= 0) {
        cows++;
      }
    }
    return this.languageService.get('bulls') + bulls + ', ' + this.languageService.get('cows') + cows;
  }

}
