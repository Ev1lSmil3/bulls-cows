import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserSignUpData } from '../signup/usersignupdata';
import { UserService } from '../user/user.service';
import * as firebase from 'firebase';
import { Router } from "@angular/router";
import { OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from '../utilities/validation/validation.service';

@Injectable()
export class GameService {

  game;
  player;
  player2Data;
  tick;
  choosen = false;
  inter;
  gameIsOver = false;
  constructor(private afa: AngularFireAuth,
    private afdb: AngularFireDatabase,
    private userService: UserService,
    private router: Router,
    private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private validationService: ValidationService) {

  }


  update(game) {
    return new Promise((resolve, reject) => {
      this.afdb.object('/game/' + game.key).set(game).then(done => { resolve(); }, err => { reject(err); })
    });
  }

  createGame() {
    this.gameIsOver = false;
    let subscription = this.afdb.list('game', {
      query: {
        orderByChild: 'status',
        equalTo: 'searching'
      }
    }).subscribe(games => {
      subscription.unsubscribe();
      if (games.length > 0) {
        if (games[0].player1 !== this.userService.currentUserData.id) {
          games[0].player2 = this.userService.currentUserData.id;
          games[0].status = 'drawing';
          this.game = games[0];

          this.update(games[0]).then(() => {
            this.router.navigate(['/game']);

            this.startListening();
          })
        }
      } else {
        let tmpGame = { player1: this.userService.currentUserData.id, player2: '', status: 'searching', key: '', player1Number: '', player2Number: '' };
        let key = this.afdb.list('game').push(tmpGame).key;
        tmpGame.key = key;
        this.update(tmpGame).then(() => {
          this.router.navigate(['/game']);
          this.game = tmpGame;
          this.startListening();
        });
      }
    });
  }

  startListening() {
    if (this.game) {
      let sub = this.afdb.object('game/' + this.game.key + '/status').subscribe(status => {
        if (this.gameIsOver) {
          sub.unsubscribe();
        } else {
          switch (status.$value) {
            case 'drawing': this.onDrawing(); break;
            case 'player1Turn': this.choosen = false; this.test1(); break;
            case 'player2Turn': this.choosen = false; this.test2(); break;
            case 'player1Won': break;
            case 'player2Won': break;
            default: null;
          }
        }
      });
    }
  }
  test1() {
    if (this.player == 1) {
      this.timer(60);
    } else {
      this.choosen = true;
    }
  }

  test2() {
    if (this.player == 2) {
      this.timer(60);
    } else {
      this.choosen = true;
    }
  }
  onDrawing() {
    this.onChanging();
    this.timer(60);
    let sub = this.afdb.object('game/' + this.game.key).subscribe(game => {
      sub.unsubscribe();
      this.game = game;
      this.player = this.game.player1 == this.userService.currentUserData.id ? 1 : 2;

      if (this.player === 2) {

        let subscription = this.afdb.list('users', {
          query: {
            orderByChild: 'id',
            equalTo: this.game.player1
          }
        }).subscribe(users => {
          subscription.unsubscribe();
          users.forEach(user => {
            this.player2Data = user;
            if (this.player2Data.image) {
              let storageRef = firebase.storage().ref();
              storageRef.child(this.player2Data.image).getDownloadURL().then(url => {
                this.player2Data.photo = url;
              });
            }

          });
        });
        return;
      }
      let subscription = this.afdb.list('users', {
        query: {
          orderByChild: 'id',
          equalTo: this.game.player2
        }
      }).subscribe(users => {
        subscription.unsubscribe();

        users.forEach(user => {

          this.player2Data = user;
          if (this.player2Data.image) {
            let storageRef = firebase.storage().ref();
            storageRef.child(this.player2Data.image).getDownloadURL().then(url => {
              this.player2Data.photo = url;
            });
          }

        });
      });
    });
    let sub1 = this.afdb.object('game/' + this.game.key).subscribe(game => {
      if (game.player1Number && game.player2Number) {
        this.choosen = true;
        if (this.inter) {
          clearInterval(this.inter);
        }
        this.game.status = 'player1Turn';
        this.update(this.game);
        sub1.unsubscribe();

      }
    });
  }
  onChanging() {
    let sub = this.afdb.object('game/' + this.game.key).subscribe(game => {
      this.game = game;
    });
    if (this.gameIsOver) {
      sub.unsubscribe();
    }
  }
  tryNumber(number) {
    let sub2 = this.afdb.object('game/' + this.game.key).subscribe(game => {
      setTimeout(() => {
        sub2.unsubscribe();
        if (this.validationService.checkTryNumber(number)) {
          if (this.game.status == 'player1Turn') {
            if (number == this.game.player2Number) {
              this.game.status = "player1Won";
              this.update(this.game);
              return;
            }
            if (!this.game.player1Turns) {
              this.game['player1Turns'] = [];
              this.game.player1Turns.push(number);
            } else {
              this.game.player1Turns.push(number);
            }
            this.game.status = 'player2Turn';
          } else if (this.game.status == 'player2Turn') {
            if (number == this.game.player1Number) {
              this.game.status = "player2Won";
              this.update(this.game);
              return;
            }
            if (!this.game.player2Turns) {
              this.game['player2Turns'] = [];
              this.game.player2Turns.push(number);
            } else {
              this.game.player2Turns.push(number);
            }
            this.game.status = 'player1Turn';
          }
          this.choosen = true; // !!!!!! 111
        }
        this.update(this.game);
      }, 300);
    });
  }
  getMyTurns() {
    if (!this.gameIsOver) {
      if (this.player == 1) {
        return this.game.player1Turns;
      } else {
        return this.game.player2Turns
      }
    }
  }
  getOpponentTurns() {
    if (!this.gameIsOver) {
      if (this.player == 1) {
        return this.game.player2Turns;
      } else {
        return this.game.player1Turns;
      }
    }
  }
  getYourNumber() {
    if (!this.gameIsOver) {
      if (this.player !== 1) {
        return this.game.player2Number.toString();
      } else {
        return this.game.player1Number.toString();
      }
    }
  }
  getOpponentNumber() {
    if (!this.gameIsOver) {
      if (this.player == 1) {
        return this.game.player2Number.toString();
      } else {
        return this.game.player1Number.toString();
      }
    }
  }
  finishGame() {
    clearInterval(this.inter);
    delete this.player2Data;
    this.router.navigate(['']);
  }
  detectLeaver() {
    if (this.game.status == 'searching') {
      this.afdb.list('game').remove(this.game.key).then(() => {
        this.router.navigate(['']);
      })
    }
    if (this.game.status == 'drawing' || this.game.status == 'player1Turn' || this.game.status == 'player2Turn') {
      if (this.player == 1) {
        this.game.status = 'player2Won';
      } else {
        this.game.status = 'player1Won';
      }
      this.update(this.game);
    }
  }
  timer(seconds) {
    this.tick = seconds;
    let timerId = setInterval(timer => {
      if (this.choosen === true) {
        clearInterval(timerId);
        return;
      }
      this.tick = this.tick - 1;
      if (this.tick <= 0 && this.tick !>= 1) {
        clearInterval(timerId);
        if (this.game.status == 'drawing') {
          if (!this.game.player1Number && !this.game.player2Number) {
            this.router.navigate(['']);
          } else if (this.player == 1 && !this.game.player1Number) {
            this.game.status = 'player2Won';
          } else if (this.player == 2 && !this.game.player2Number) {
            this.game.status = 'player1Won';
          }
        } else if (this.game.status == 'player1Turn' && this.player == 1) {
          this.game.status = 'player2Turn';
          clearInterval(timerId);
        } else if (this.game.status == 'player2Turn' && this.player == 2) {
          this.game.status = 'player1Turn';
          clearInterval(timerId);
        }
        this.update(this.game);
      }
    }, 1000);
    this.inter = timerId;

  }
}

