import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ProfileImageComponent } from '../profile-image/profile-image.component';
import { LanguageService } from '../lang/language.service';
import { FirebaseApp } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { UserService } from '../user/user.service';
import { ValidationService } from '../utilities/validation/validation.service';
import { GameService } from './game.service';
import { TransferPipe } from './transfer.pipe';
import { ResultComponent } from '../result/result.component';
import { Router } from "@angular/router";
import { DialogService } from 'ng2-bootstrap-modal';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private languageService: LanguageService,
    private firebaseApp: FirebaseApp,
    private afdb: AngularFireDatabase,
    private userService: UserService,
    private validationService: ValidationService,
    private gameService: GameService,
    private router: Router) { }

  number;
  tryNumber;
  focusOutNumber = false;
  focusOutTryNumber = false;

  ngbModalOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  }

  ngOnInit() {
    if (!this.gameService.game) {
      this.router.navigate(['']);
    } else {
      let sub = this.afdb.object('game/' + this.gameService.game.key + '/status').subscribe(status => {
        if (status.$value == 'player1Won' || status.$value == 'player2Won') {
          this.gameService.gameIsOver = true;
          this.gameService.choosen = false;
          clearInterval(this.gameService.inter);
          const modalRef = this.modalService.open(ResultComponent, this.ngbModalOptions);
          sub.unsubscribe();
        }
      });
    }
  }

  setNumber(number) {
    if (this.gameService.player == 1 && this.validationService.checkNumber(number) && !this.gameService.game.player1Number) {
      this.gameService.game.player1Number = number;
      this.gameService.choosen = true;
      this.gameService.update(this.gameService.game);
    } else if (this.gameService.player == 2 && this.validationService.checkNumber(number) && !this.gameService.game.player2Number) {
      this.gameService.game.player2Number = number;
      this.gameService.choosen = true;
      this.gameService.update(this.gameService.game);
    }
  }

}
