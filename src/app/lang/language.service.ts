import { Injectable, OnInit } from '@angular/core';
import { language } from './lang'

@Injectable()
export class LanguageService implements OnInit {

  lang: string = this.lang = localStorage.getItem('lang') ? localStorage.getItem('lang') : "en";
  language = {};

  constructor() {this.language = language; }

  ngOnInit() {  }

  get(name: string): string {
     return language[name][this.lang]
  }

}
