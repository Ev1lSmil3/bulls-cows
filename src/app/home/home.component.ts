import { Component, OnInit } from '@angular/core';
import { ChooseLanguageComponent } from '../choose-language/choose-language.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GameService } from '../game/game.service';
import { UserService } from '../user/user.service';
import { LanguageService } from '../lang/language.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private modalService: NgbModal,
              private activeModal: NgbActiveModal,
              private gameService: GameService,
              private userService: UserService,
              private languageService: LanguageService) { }

  ngOnInit() {
  }

    showChooseLanguage() {
    const modalRef = this.modalService.open(ChooseLanguageComponent);
  }

}
