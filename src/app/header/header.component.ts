import { Component, OnInit } from '@angular/core';
import { DialogService } from 'ng2-bootstrap-modal';
import { ChooseLanguageComponent } from '../choose-language/choose-language.component';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LanguageService } from '../lang/language.service';
import { UserService } from '../user/user.service';
import { AuthService } from '../user/auth.service';
import { Router } from "@angular/router";
import { GameService } from '../game/game.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
profile = this.languageService.get('profile');
home = this.languageService.get('home');
button;
  constructor(private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private languageService: LanguageService,
    private userService: UserService,
    private authService: AuthService,
    private gameService: GameService,
    private router: Router) {
    this.router.events.subscribe((profile) => {
      if (profile['url'] == '/profile') {
        this.button = this.home;
      } else {
        this.button = this.profile;
      }
    });
  }
redirect() {
  if (this.button == this.profile) {
    this.router.navigate(['profile']);
  } else {
    this.router.navigate(['']);
  }
}
showButton() {
    if (this.profile == this.button) {
    return this.languageService.get('profile');
  } else {
    return this.languageService.get('home');
  }
}
  ngOnInit() {
    if (!localStorage.getItem('lang')) {
      const modalRef = this.modalService.open(ChooseLanguageComponent);
    }
  }

  langEng() {
    localStorage.setItem('lang', 'en');
    this.languageService.lang = 'en';
  }

  langRus() {
    localStorage.setItem('lang', 'ru');
    this.languageService.lang = 'ru';
  }

  showLogin() {
    const modalRef = this.modalService.open(LoginComponent);
    modalRef.componentInstance.test = "test";
  }

  showSignUp() {
    const modalRef = this.modalService.open(SignupComponent);
  }
  logOut() {
    this.authService.logOut();
  }
}
